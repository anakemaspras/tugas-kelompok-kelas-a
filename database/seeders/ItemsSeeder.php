<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class itemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'items_id' => 1,
            'name' => 'Kopi Kapal Api',
            'harga' => '5000',
            'stock' => '3',
            'goods_id' => 1,
        ]);
        DB::table('items')->insert([
            'items_id' => 2,
            'name' => 'Kopi Susu Nescafe',
            'harga' => '2000',
            'stock' => '2',
            'goods_id' => 1,
        ]);
        DB::table('items')->insert([
            'items_id' => 3,
            'name' => 'Gula Gulaku',
            'harga' => '5000',
            'stock' => '3',
            'goods_id' => 2,
        ]);
        DB::table('items')->insert([
            'items_id' => 4,
            'name' => 'Susu Dancow Coklat',
            'harga' => '2000',
            'stock' => '2',
            'goods_id' => 3,
        ]);
        DB::table('items')->insert([
            'items_id' => 5,
            'name' => '	Garam Kapal',
            'harga' => '2000',
            'stock' => '2',
            'goods_id' => 4,
        ]);
        DB::table('items')->insert([
            'items_id' => 6,
            'name' => 'Garam Bata',
            'harga' => '3000',
            'stock' => '2',
            'goods_id' => 4,
        ]);
    }
}
