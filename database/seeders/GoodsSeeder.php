<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class GoodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goods')->insert([
            'goods_id' => 1,
            'name' => 'Kopi',
            'stock' => '5',
        ]);
        DB::table('goods')->insert([
            'goods_id' => 2,
            'name' => 'Gula',
            'stock' => '3',
        ]);
        DB::table('goods')->insert([
            'goods_id' => 3,
            'name' => 'Susu',
            'stock' => '4',
        ]);
        DB::table('goods')->insert([
            'goods_id' => 4,
            'name' => 'Garam',
            'stock' => '2',
        ]);
    }
}
