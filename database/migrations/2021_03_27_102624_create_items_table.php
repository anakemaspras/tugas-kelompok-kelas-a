<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id('items_id');
            $table->string('name');
            $table->string('price');
            $table->string('stock');
            $table->integer('goods_id')->unsigned()->index();
            $table->foreign('goods_id')->references('goods_id')->on('goods')->onDelete('cascade');
            $table->timestamps();
        });
        // Schema::table('items', function($table) {
        //     $table->foreign('goods_id')->references('goods_id')->on('goods')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
