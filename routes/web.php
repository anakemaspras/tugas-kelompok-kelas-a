<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\GoodsController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\UsersController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [AuthController::class, 'home'])->name('login');
Route::post('/', [AuthController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [PagesController::class, 'home']);

    Route::get('/dashboard', [PagesController::class, 'home']);

    Route::get('/listBarang', [GoodsController::class, 'index']);

    Route::get('/detailBarang', [ItemsController::class, 'index']);
    Route::get('/detailBarang/create', [ItemsController::class, 'create']);
    Route::post('/detailBarang', [ItemsController::class, 'store']);
    Route::get('/detailBarang/{good}', [ItemsController::class, 'show']);

    Route::get('/listUser', [UsersController::class, 'index']);
    Route::get('/listUser/create', [UsersController::class, 'create']);
    Route::post('/listUser', [UsersController::class, 'store']);
    
    Route::get('/users', [UsersController::class, 'users']);

    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});
