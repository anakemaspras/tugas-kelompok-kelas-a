@extends('layout/template')

@section('title', 'Tambah Item')

@section('container')
    <h1>Tambah Item</h1>
    <form method="POST" action="/listItem">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama Item</label>
            <input type="text" class="form-control" id="name">
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Harga</label>
            <input type="text" class="form-control" id="price">
        </div>
        <div class="mb-3">
            <label for="stock" class="form-label">Jumlah</label>
            <input type="text" class="form-control" id="stock">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
