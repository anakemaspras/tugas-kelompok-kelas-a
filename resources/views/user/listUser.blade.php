@extends('layout/template')

@section('title', 'List User')

@section('container')
  <h1>List User</h1>
    <table>
        <tr>
            <td><a href="" class="btn btn-primary" role="button">Cari</a></td>
            <td><a href="/listUser/create" class="btn btn-success" role="button">Tambah</a></td>
            <td><a href="" class="btn btn-danger" role="button">Hapus</a></td>
        </tr>
    </table>
    <table class="table table-dark table-striped">
      <thead>
          <tr>
              <th scope="col">#</th>
              <th scope="col">ID_User</th>
              <th scope="col">Nama</th>
              <th scope="col">Email</th>
          </tr>
      </thead>
      <tbody>
          @foreach ($users as $user)
              <tr>
                  <th scope="row">{{ $loop->iteration }}</th>
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
              </tr>
          @endforeach
      </tbody>
  </table>
@endsection
