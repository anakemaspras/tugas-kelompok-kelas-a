@extends('layout/template')

@section('title', 'Tambah User')

@section('container')
    <h1>Tambah User</h1>
    <form method="POST" action="/listUser">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama</label>
            <input type="text" class="form-control" id="name">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email">
        </div>
        <div class="mb-3">
            <label for="pasword" class="form-label">Password</label>
            <input type="password" class="form-control" id="pasword">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
