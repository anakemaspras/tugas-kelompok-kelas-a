@extends('layout/template')

@section('title', 'Tugas Kelompok')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Welcome to this page!</h1>
                <h2>Anggota Kelompok</h2>
                <table  class="table-dark">
                    <tr>
                        <td>Ilham Hananto Wibisono</td>
                        <td></td>
                        <td>185150400111029</td>
                    </tr>
                    <tr>
                        <td>Junjungan Siregar</td>
                        <td></td>
                        <td>185150400111052</td>
                    </tr>
                    <tr>
                        <td>Muhammad Daffa Arga</td>
                        <td></td>
                        <td>185150400111053</td>
                    </tr>
                    <tr>
                        <td>Rizqi Ahmad Abdillah</td>
                        <td></td>
                        <td>185150407111018</td>
                    </tr>
                </table>
            </div>
        </div>
    </div> 
@endsection
